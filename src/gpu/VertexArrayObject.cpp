#include "gpu/VertexArrayObject.h"

using namespace k;
using namespace gpu;

VertexArrayObject::VertexArrayObject() { glCreateVertexArrays(1, &_id); }
VertexArrayObject::~VertexArrayObject() { if(_id) glDeleteVertexArrays(1, &_id); }

VertexArrayObject::VertexArrayObject(VertexArrayObject&& that)
{
	_id = that._id;

	that._id = 0;
}

VertexArrayObject& VertexArrayObject::operator=(VertexArrayObject&& that)
{
	std::swap(_id, that._id);
	return *this;
}


void VertexArrayObject::AttachVertexBuffer(const BufferObject &buffer, uint32 bindingIndex, intptr offset, int32 stride, const std::vector<VertexAttribute> &attr)
{
	glVertexArrayVertexBuffer(_id, bindingIndex, buffer.GetId(), offset, stride);
	//glVertexArrayBindingDivisor(_id, bindingIndex, 0);
	for (uint32 i = 0; i < attr.size(); ++i)
	{
		glVertexArrayAttribBinding(_id, attr[i].index, bindingIndex);
		glVertexArrayAttribFormat(_id, attr[i].index, attr[i].count, (GLenum)attr[i].type, attr[i].normalized, attr[i].offset);
		glEnableVertexArrayAttrib(_id, attr[i].index);
	}
}

void VertexArrayObject::AttachElementBuffer(const BufferObject &buffer)
{
	glVertexArrayElementBuffer(_id, buffer.GetId());
}

void VertexArrayObject::Bind() { glBindVertexArray(_id); }
void VertexArrayObject::Unbind() { glBindVertexArray(0); }