#include "gpu/TextureObject.h"

using namespace k;
using namespace gpu;

#define DEF_FORMAT k::gpu::gpu_tex_format::rgb_b8
#define DEF_MIN_FILTER k::gpu::gpu_tex_filter::linear_mipmap_linear
#define DEF_MAG_FILTER k::gpu::gpu_tex_filter::linear
#define DEF_WRAP k::gpu::gpu_tex_wrapping::repeat

TextureObject::TextureObject(gpu_tex_target target) :
	_target(target), _format(DEF_FORMAT), _width(0), _height(0), _minFilter(DEF_MIN_FILTER), _magFilter(DEF_MAG_FILTER),
	_wrappingS(DEF_WRAP), _wrappingT(DEF_WRAP), _count(0)
{
	glCreateTextures((GLenum)target, 1, &_id);
}

TextureObject::~TextureObject(){ if(_id) glDeleteTextures(1, &_id); }

TextureObject::TextureObject(TextureObject&& that)
{
	_id = that._id;
	_target = that._target;
	_format = that._format;
	_width = that._width;
	_height = that._height;
	_minFilter = that._minFilter;
	_magFilter = that._magFilter;
	_wrappingS = that._wrappingS;
	_wrappingT = that._wrappingT;
	_count = that._count;

	that._id = 0;
	that._format = DEF_FORMAT;
	that._width = 0;
	that._height = 0;
	that._minFilter = DEF_MIN_FILTER;
	that._magFilter = DEF_MAG_FILTER;
	that._wrappingS = DEF_WRAP;
	that._wrappingT = DEF_WRAP;
	that._count = 0;
}

TextureObject& TextureObject::operator=(TextureObject&& that)
{
	std::swap(_id, that._id);
	std::swap(_target, that._target);
	std::swap(_format, that._format);
	std::swap(_width, that._width);
	std::swap(_height, that._height);
	std::swap(_minFilter, that._minFilter);
	std::swap(_magFilter, that._magFilter);
	std::swap(_wrappingS, that._wrappingS);
	std::swap(_wrappingT, that._wrappingT);
	std::swap(_count, that._count);
	return *this;
}

void TextureObject::Allocate(gpu_tex_format format, int32 width, int32 height, gpu_tex_filter min, gpu_tex_filter mag, gpu_tex_wrapping s, gpu_tex_wrapping t, int32 count)
{
	switch (_target)
	{
	case gpu_tex_target::buffer:
	case gpu_tex_target::texture:
	case gpu_tex_target::cubemap:
		glTextureStorage2D(_id, 1, (GLenum)format, width, height);
		_count = 1;
		break;
	case gpu_tex_target::texure_array:
		glTextureStorage3D(_id, 1, (GLenum)format, width, height, count);
		break;
	}

	glTextureParameteri(_id, GL_TEXTURE_MIN_FILTER, (GLenum)min);
	glTextureParameteri(_id, GL_TEXTURE_MAG_FILTER, (GLenum)mag);
	glTextureParameteri(_id, GL_TEXTURE_WRAP_S, (GLenum)s);
	glTextureParameteri(_id, GL_TEXTURE_WRAP_T, (GLenum)t);

}

void TextureObject::LoadData(int32 x, int32 y, int32 w, int32 h, gpu_pixel_format format, gpu_type type, const void* pixels)
{
	glTextureSubImage2D(_id, 0, x, y, w, h, (GLenum)format, (GLenum)type, pixels);
}

void TextureObject::LoadData(int32 x, int32 y, int32 w, int32 h, int32 layer, gpu_pixel_format format, gpu_type type, const void* pixels)
{
	glTextureSubImage3D(_id, 0, x, y, layer, w, h, 1, (GLenum)format, (GLenum)type, pixels);
}

void TextureObject::LoadData(int32 x, int32 y, int32 w, int32 h, gpu_cube_map side, gpu_pixel_format format, gpu_type type, const void* pixels)
{
	int layer = (int)side - (int)gpu_cube_map::pos_x;
	glTextureSubImage3D(_id, 0, x, y, layer, w, h, 1, (GLenum)format, (GLenum)type, pixels);
}

void TextureObject::GenerateMipmap()
{
	glGenerateTextureMipmap(_id);
}

void TextureObject::Bind(gpu_tex_unit unit)
{
	glBindTextureUnit((uint32)unit, _id);
}

uint32 TextureObject::GetId() const
{
	return _id;
}