#include "gpu/FrameBufferObject.h"

using namespace k;
using namespace gpu;

FrameBufferObject::FrameBufferObject() { glCreateFramebuffers(1, &_id); }
FrameBufferObject::~FrameBufferObject() { glDeleteFramebuffers(1, &_id); }

FrameBufferObject::FrameBufferObject(FrameBufferObject&& that)
{
	_id = that._id;

	that._id = 0;
}

FrameBufferObject& FrameBufferObject::operator=(FrameBufferObject&& that)
{
	std::swap(_id, that._id);
	return *this;
}

void FrameBufferObject::AttachTexture(gpu_fbo_attachment attachment, const TextureObject &texture, int32 layer)
{
	glNamedFramebufferTextureLayer(_id, (GLenum)attachment, texture.GetId(), 0, layer);
}

void FrameBufferObject::AttachTexture(gpu_fbo_attachment attachment, const TextureObject &texture)
{
	glNamedFramebufferTexture(_id, (GLenum)attachment, texture.GetId(), 0);
}

void FrameBufferObject::DetachTexture(gpu_fbo_attachment attachment)
{
	glNamedFramebufferTexture(_id, (GLenum)attachment, 0, 0);
}

bool FrameBufferObject::Validate()
{
	GLenum status = glCheckNamedFramebufferStatus(_id, GL_FRAMEBUFFER);

	if (status == GL_FRAMEBUFFER_COMPLETE)
		return true;

	if (status == GL_FRAMEBUFFER_UNDEFINED)
		k::MessageBox("Specified framebuffer is the default read or draw framebuffer, but the default framebuffer does not exist.", "GL_FRAMEBUFFER_UNDEFINED");
	else if (status == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
		k::MessageBox("Any of the framebuffer attachment points are framebuffer incomplete.", "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
	else if (status == GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)
		k::MessageBox("The framebuffer does not have at least one image attached to it.", "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
	else if (status == GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER)
		k::MessageBox("The value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for any color attachment point(s) named by GL_DRAW_BUFFERi.", "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER");
	else if (status == GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER)
		k::MessageBox("GL_READ_BUFFER is not GL_NONE and the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for the color attachment point named by GL_READ_BUFFER.", "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER");
	else if (status == GL_FRAMEBUFFER_UNSUPPORTED)
		k::MessageBox("The combination of internal formats of the attached images violates an implementation-dependent set of restrictions.");
	else if (status == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE)
		k::MessageBox("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE", "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE");
	else if (status == GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS)
		k::MessageBox("Any framebuffer attachment is layered, and any populated attachment is not layered, or if all populated color attachments are not from textures of the same target.");

	return false;
}

void FrameBufferObject::Bind(const std::vector<gpu_fbo_attachment> &attachments)
{
	glBindFramebuffer(GL_FRAMEBUFFER, _id);
	glNamedFramebufferDrawBuffers(_id, attachments.size(), (GLenum*)&attachments[0]);
}