#ifndef __K_GPU_TEXTURE_OBJECT_H__
#define __K_GPU_TEXTURE_OBJECT_H__
#include "gpu.h"

namespace k
{
	namespace gpu
	{
		enum struct gpu_tex_target : GLenum
		{
			texture = GL_TEXTURE_2D,
			texure_array = GL_TEXTURE_2D_ARRAY,
			cubemap = GL_TEXTURE_CUBE_MAP,
			//cubemap_array = GL_TEXTURE_CUBE_MAP_ARRAY,
			buffer = GL_TEXTURE_BUFFER,
		};

		enum struct gpu_tex_format : GLenum
		{
			depth_f32 = GL_DEPTH_COMPONENT32F,
			rgb_b8 = GL_RGB8,
			rgba_b8 = GL_RGBA8,
			srgb_b8 = GL_SRGB8,
			srgba_b8 = GL_SRGB8_ALPHA8,
			rgba_f32 = GL_RGBA32F,
		};

		enum struct gpu_tex_filter : GLenum
		{
			nearest = GL_NEAREST,
			linear = GL_LINEAR,
			nearest_mipmap_nearest = GL_NEAREST_MIPMAP_NEAREST,
			linear_mipmap_nearest = GL_LINEAR_MIPMAP_NEAREST,
			nearest_mipmap_linear = GL_NEAREST_MIPMAP_LINEAR,
			linear_mipmap_linear = GL_LINEAR_MIPMAP_LINEAR,
		};

		enum struct gpu_tex_wrapping : GLenum
		{
			repeat = GL_REPEAT,
			clamp_to_edge = GL_CLAMP_TO_EDGE,
			clamp_to_border = GL_CLAMP_TO_BORDER,
			mirrored_repeat = GL_MIRRORED_REPEAT,
			//gpu_mirror_clamp = GL_MIRROR_CLAMP,
			//gpu_mirror_clamp_to_edge = GL_MIRROR_CLAMP_TO_EDGE,
			//gpu_mirror_clamp_to_border = GL_MIRROR_CLAMP_TO_BORDER
		};

		enum struct gpu_pixel_format : GLenum
		{
			rgb = GL_RGB,
			bgr = GL_BGR,
			rgba = GL_RGBA,
			bgra = GL_BGRA,
		};

		enum struct gpu_cube_map : GLenum
		{
			pos_x = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			neg_x = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
			pos_y = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
			neg_y = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
			pos_z = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
			neg_z = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		};

		enum struct gpu_tex_unit : uint32
		{
			unit0 = 0,
			unit1,
			unit2,
			unit3,
			unit4,
			unit5,
			unit6,
			unit7,
		};

		class TextureObject
		{
		public:
			TextureObject(gpu_tex_target target);
			~TextureObject();

			TextureObject(TextureObject&& that);
			TextureObject& operator=(TextureObject&& that);

			TextureObject(const TextureObject& that) = delete;
			TextureObject& operator=(const TextureObject&) = delete;

			void Allocate(gpu_tex_format format, int32 width, int32 height, 
				gpu_tex_filter min = gpu_tex_filter::linear_mipmap_linear, gpu_tex_filter mag = gpu_tex_filter::linear,
				gpu_tex_wrapping s = gpu_tex_wrapping::repeat, gpu_tex_wrapping t = gpu_tex_wrapping::repeat, int32 count = 1);

			void LoadData(int32 x, int32 y, int32 w, int32 h, gpu_pixel_format format, gpu_type type, const void* pixels);
			void LoadData(int32 x, int32 y, int32 w, int32 h, int32 layer, gpu_pixel_format format, gpu_type type, const void* pixels);
			void LoadData(int32 x, int32 y, int32 w, int32 h, gpu_cube_map side, gpu_pixel_format format, gpu_type type, const void* pixels);

			void GenerateMipmap();

			void Bind(gpu_tex_unit unit);

			uint32 GetId() const;
		private:
			gpu_tex_target		_target;
			gpu_tex_format		_format;
			int32				_width, 
								_height;
			gpu_tex_filter		_minFilter;
			gpu_tex_filter		_magFilter;
			gpu_tex_wrapping	_wrappingS;
			gpu_tex_wrapping	_wrappingT;
			int32				_count;

			uint32				_id;
		};
	}
}
#endif // !__K_DEVICE_TEXTURE_OBJECT_H__

