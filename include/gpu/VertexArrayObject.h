#ifndef __K_GPU_VERTEX_ARRAY_OBJECT_H__
#define __K_GPU_VERTEX_ARRAY_OBJECT_H__
#include "gpu.h"
#include "BufferObject.h"

namespace k
{
	namespace gpu
	{
		struct VertexAttribute
		{
			uint32 index; // (location = x) in shader
			uint32 count;
			gpu_type type;
			uint32 offset; // offset in BYTE
			bool normalized;
		};

		class VertexArrayObject
		{
		public:
			VertexArrayObject();
			~VertexArrayObject();

			VertexArrayObject(VertexArrayObject &&that);
			VertexArrayObject& operator=(VertexArrayObject &&that);

			VertexArrayObject(const VertexArrayObject &that) = delete;
			VertexArrayObject& operator=(const VertexArrayObject&) = delete;
			
			void AttachVertexBuffer(const BufferObject &buffer, uint32 bindingIndex, intptr offset, int32 stride, const std::vector<VertexAttribute> &attr);
			void AttachElementBuffer(const BufferObject &buffer);

			void Bind();
			static void Unbind();
		private:
			uint32 _id;
		};
	}
}
#endif // !__K_GPU_VERTEX_ARRAY_OBJECT_H__
