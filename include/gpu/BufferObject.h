#ifndef __K_GPU_BUFFER_OBJECT_H__
#define __K_GPU_BUFFER_OBJECT_H__
#include "gpu.h"

namespace k
{
	namespace gpu
	{
		enum struct gpu_buffer_target : GLenum
		{
			array = GL_ARRAY_BUFFER,
			//atomic_counter = GL_ATOMIC_COUNTER_BUFFER,
			copy_read = GL_COPY_READ_BUFFER,
			copy_write = GL_COPY_WRITE_BUFFER,
			//dispatch_indirect = GL_DISPATCH_INDIRECT_BUFFER,
			//draw_indirec = GL_DRAW_INDIRECT_BUFFER,
			element_array = GL_ELEMENT_ARRAY_BUFFER,
			pixel_pack = GL_PIXEL_PACK_BUFFER,
			pixel_unpack = GL_PIXEL_UNPACK_BUFFER,
			//query = GL_QUERY_BUFFER,
			shader_storage = GL_SHADER_STORAGE_BUFFER,
			texture = GL_TEXTURE_BUFFER,
			transform_feedback = GL_TRANSFORM_FEEDBACK_BUFFER,
			uniform = GL_UNIFORM_BUFFER,
		};

		enum struct gpu_buffer_usage : GLenum
		{
			stream_draw = GL_STREAM_DRAW,
			stream_read = GL_STREAM_READ,
			stream_copy = GL_STREAM_COPY,
			static_draw = GL_STATIC_DRAW,
			static_read = GL_STATIC_READ,
			static_copy = GL_STATIC_COPY,
			dynamic_draw = GL_DYNAMIC_DRAW,
			dynamic_read = GL_DYNAMIC_READ,
			dynamic_copy = GL_DYNAMIC_COPY,
		};

		class BufferObject
		{
		public:
			BufferObject();
			~BufferObject();

			BufferObject(BufferObject&& that);
			BufferObject& operator=(BufferObject&& that);

			BufferObject(const BufferObject& that) = delete;
			BufferObject& operator=(const BufferObject&) = delete;

			void Allocate(size_t size, gpu_buffer_target target = gpu_buffer_target::array, gpu_buffer_usage usageMode = gpu_buffer_usage::static_draw, const void* data = nullptr);
			void LoadData(intptr offset, size_t size, const void* data);

			void Bind();
			static void Unbind(gpu_buffer_target target);
			void BindToIndexedBuffer(uint32 bufferUnit);

			uint32 GetId() const;
		private:
			uint32 _id;
			gpu_buffer_target _target;
		};
	}
}
#endif // !__K_DEVICE_BUFFER_H__

