#ifndef __K_GPU_PIPELINE_OBJECT_H__
#define __K_GPU_PIPELINE_OBJECT_H__
#include "gpu.h"

namespace k
{
	namespace gpu
	{
		enum struct gpu_shader_type : GLenum
		{
			vertex = GL_VERTEX_SHADER,
			fragment = GL_FRAGMENT_SHADER,
			geometry = GL_GEOMETRY_SHADER,
			//tess_control = GL_TESS_CONTROL_SHADER, 
			//tess_evaluation = GL_TESS_EVALUATION_SHADER, 
			//compute = GL_COMPUTE_SHADER, 
		};

		enum struct gpu_shader_bitfield : GLbitfield
		{
			vertex = GL_VERTEX_SHADER_BIT,
			fragment = GL_FRAGMENT_SHADER_BIT,
			geometry = GL_GEOMETRY_SHADER_BIT,
			//tess_control = GL_TESS_CONTROL_SHADER_BIT, 
			//tess_evaluation = GL_TESS_EVALUATION_SHADER_BIT, 
			//compute = GL_COMPUTE_SHADER_BIT,
		};

		class Shader
		{
		public:
			Shader(gpu_shader_type type, const char* src);
			~Shader();

			Shader(Shader&& that);
			Shader& operator=(Shader&& that);

			Shader(const Shader& that) = delete;
			Shader& operator=(const Shader&) = delete;

			uint32 GetID() const;
			gpu_shader_type GetType() const;

			void BindShaderStorage(const char* blockName, uint32 bufferUnit);
			void BindUniformBuffer(const char* blockName, uint32 bufferUnit);

			inline void Uniform1i (uint32 location, int32 value) { glProgramUniform1i(_id, location, value); }
			inline void Uniform1f (uint32 location, float value) { glProgramUniform1f(_id, location, value); }
			inline void Uniform1ui(uint32 location, uint32 value) { glProgramUniform1ui(_id, location, value); }

			inline void Uniform1iv (uint32 location, int32 count, const int32* value) { glProgramUniform1iv(_id, location, count, value); }
			inline void Uniform1fv (uint32 location, int32 count, const float* value) { glProgramUniform1fv(_id, location, count, value); }
			inline void Uniform1uiv(uint32 location, int32 count, const uint32* value) { glProgramUniform1uiv(_id, location, count, value); }
			
			inline void Uniform2iv (uint32 location, int32 count, const int32* value) { glProgramUniform2iv(_id, location, count, value); }
			inline void Uniform2fv (uint32 location, int32 count, const float* value) { glProgramUniform2fv(_id, location, count, value); }
			inline void Uniform2uiv(uint32 location, int32 count, const uint32* value) { glProgramUniform2uiv(_id, location, count, value); }
			
			inline void Uniform3iv (uint32 location, int32 count, const int32* value) { glProgramUniform3iv(_id, location, count, value); }
			inline void Uniform3fv (uint32 location, int32 count, const float* value) { glProgramUniform3fv(_id, location, count, value); }
			inline void Uniform3uiv(uint32 location, int32 count, const uint32* value) { glProgramUniform3uiv(_id, location, count, value); }
			
			inline void Uniform4iv (uint32 location, int32 count, const int32* value) { glProgramUniform4iv(_id, location, count, value); }
			inline void Uniform4fv (uint32 location, int32 count, const float* value) { glProgramUniform4fv(_id, location, count, value); }
			inline void Uniform4uiv(uint32 location, int32 count, const uint32* value) { glProgramUniform4uiv(_id, location, count, value); }
			
			inline void UniformMatrix2fv(uint32 location, int32 count, const float* value) { glProgramUniformMatrix2fv(_id, location, count, 0, value); }
			inline void UniformMatrix3fv(uint32 location, int32 count, const float* value) { glProgramUniformMatrix3fv(_id, location, count, 0, value); }
			inline void UniformMatrix4fv(uint32 location, int32 count, const float* value) { glProgramUniformMatrix4fv(_id, location, count, 0, value); }
			
		private:
			uint32 _id;
			gpu_shader_type _type;
		};

		class PipelineObject
		{
		public:
			PipelineObject();
			~PipelineObject();

			PipelineObject(PipelineObject&& that);
			PipelineObject& operator=(PipelineObject&& that);

			PipelineObject(const PipelineObject& that) = delete;
			PipelineObject& operator=(const PipelineObject&) = delete;

			void AttachShader(const Shader &shader);
			void DetachShader(gpu_shader_type stage);
			bool Validate();

			void Bind();
			static void Unbind();
		private:
			uint32 _id;
		};
	}
}
#endif