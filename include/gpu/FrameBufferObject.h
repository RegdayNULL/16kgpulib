#ifndef __K_GPU_FRAMEBUFFER_OBJECT_H__
#define __K_GPU_FRAMEBUFFER_OBJECT_H__
#include "gpu.h"
#include "TextureObject.h"

namespace k
{
	namespace gpu
	{
		enum struct gpu_fbo_attachment : GLenum
		{
			depth = GL_DEPTH_ATTACHMENT,
			color0 = GL_COLOR_ATTACHMENT0,
			color1 = GL_COLOR_ATTACHMENT1,
			color2 = GL_COLOR_ATTACHMENT2,
			color3 = GL_COLOR_ATTACHMENT3,
			color4 = GL_COLOR_ATTACHMENT4,
			color5 = GL_COLOR_ATTACHMENT5,
			color6 = GL_COLOR_ATTACHMENT6,
			color7 = GL_COLOR_ATTACHMENT7,
		};

		class FrameBufferObject
		{
		public:
			FrameBufferObject();
			~FrameBufferObject();

			FrameBufferObject(FrameBufferObject&& that);
			FrameBufferObject& operator=(FrameBufferObject&& that);

			FrameBufferObject(const FrameBufferObject& that) = delete;
			FrameBufferObject& operator=(const FrameBufferObject&) = delete;

			void AttachTexture(gpu_fbo_attachment attachment, const TextureObject &texture, int32 layer);
			void AttachTexture(gpu_fbo_attachment attachment, const TextureObject &texture);
			void DetachTexture(gpu_fbo_attachment attachment);

			bool Validate();

			void Bind(const std::vector<gpu_fbo_attachment> &attachments);
			static inline bool Unbind() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }
		private:
			uint32 _id;
		};
	}
}

#endif // !__K_GPU_FRAMEBUFFER_OBJECT_H__

